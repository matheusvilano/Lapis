# Lapis

## Description
Lapis is a single-player, third-person marble time-attack platformer. Play through the levels and try to achieve the best time!

## Purpose
This project was created primarily as a way to learn Unity's new Visual Scripting system (known as "Bolt" prior to version 2021). The goal is to identify the pros and cons of the language, while determining an efficient workflow.

## Authors

### Matheus Vilano
- Generalist Programmer
- Game Designer
- Sound Designer
- Level Designer
- Visual Artist

## Tools
- Unity
- Reaper
- Blender

## Gameplay

### Actions
| Name  | Description                                   |
|:----- |:--------------------------------------------- |
| Move  | Physics-based. The player can move anytime.   |
| Jump  | Only available when grounded (via raycast).   |
| Burst | Cooldown-based. Can be used as a second jump. |

### Collectibles / Consumables
| Name               | Description                                                                 |
|:------------------ |:--------------------------------------------------------------------------- |
| Boost              | Forward only. Consumed on input.                                            |
| Bomb               | Used to destroy obstacles/enemies. Slight aim assist.                       |
| Shield             | Auto-applied. Will stay active until the player is damaged. Does not stack. |
| Anti-Gravity Field | Time-based; effect wears of after X seconds.                                |

### Obstacles / Tools
| Name               | Description                                                                        |
|:------------------ |:---------------------------------------------------------------------------------- |
| Bumper             | Shoots player away from it (angle reflected perfectly).                            |
| Trampoline         | Boosts player up (similar to Bumper, but always up).                               |
| Black Hole         | Sucks player in. There is a point-of-no-return (event horizon).                    |
| Electrified Sphere | AI magnetized/electrified spheres that will attempt to hit and destroy the player. |

### Surfaces
- Regular
- Rocky (bumpy)
- Icy (slippery)
- Slimey (sticky)
- Fiery (explosive; time-based)

## Customization
The player can select what Sphere texture to use.