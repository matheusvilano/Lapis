/*
 * PlayerStyleManager.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;

namespace Lapis.Player
{
    /// <summary>
    /// Manager class for player style preferences.
    /// </summary>
    public class PlayerStyleManager : MonoBehaviour
    {
        /// <summary>
        /// The Player's mesh renderer.
        /// </summary>
        private MeshRenderer _renderer;

        /// <summary>
        /// Used to initialize variales.
        /// </summary>
        private void Awake()
        {
            _renderer = GetComponent<MeshRenderer>();
        }

        /// <summary>
        /// Used to set the style at the very beginning of the game.
        /// </summary>
        private void Start()
        {
            SetStyle();
        }

        /// <summary>
        /// Allows setting a new style for the player.
        /// </summary>
        public void SetStyle()
        {
            _renderer.material = Resources.Load(PlayerPrefs.GetString("PlayerSkin")) as Material ?? _renderer.material;
        }

        /// <summary>
        /// Allows settings a new style for the player.
        /// </summary>
        /// <param name="style">The new style.</param>
        public void SetStyle(Material style)
        {
            _renderer.material = style;
        }
    }
}
