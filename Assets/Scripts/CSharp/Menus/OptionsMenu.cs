/*
 * OptionsMenu.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Events;
using Lapis.Helpers;

namespace Lapis.Menus
{
    /// <summary>
    /// The options menu. This class handles registering callbacks and other functionalities.
    /// </summary>
    public class OptionsMenu : MonoBehaviour, IVisibility // TODO: create parent Menu class
    {
        /// <summary>
        /// The default audio slider value. Audio sliders should ideally start at full-volume (1 or 100%).
        /// </summary>
        private const float DefaultAudioSliderValue = 100.0f;
        /// <summary>
        /// The default gamma slider value.
        /// </summary>
        private const float DefaultGammaSliderValue = 50.0f;
    
        [Header("State")] 
        [SerializeField, Tooltip("Whether or not this menu should be readily visible.")] 
        private bool willStartVisible;
        
        [Header("Style")]
        [SerializeField, Tooltip("The opacity of the button when focused.")] 
        float buttonOpacityFocused = 1.0f;
        [SerializeField, Tooltip("The original opacity of the buttons.")] 
        float buttonOpacityUnfocused = 0.5f;
        
        [Header("Values")]
        [SerializeField, Tooltip("Invoked whenever the Master slider value is changed.")] 
        private UnityEvent<float> masterSliderValueChanged;
        [SerializeField, Tooltip("Invoked whenever the SFX slider value is changed.")] 
        private UnityEvent<float> sfxSliderValueChanged;
        [SerializeField, Tooltip("Invoked whenever the Music slider value is changed.")] 
        private UnityEvent<float> musicSliderValueChanged;
        
        [Header("Focus")]
        [SerializeField, Tooltip("Invoked whenever the Player Skin dropdown field gains focus.")] 
        private UnityEvent playerSkinDropdownFieldGainedFocus;
        [SerializeField, Tooltip("Invoked whenever the Master slider gains focus.")] 
        private UnityEvent masterSliderGainedFocus;
        [SerializeField, Tooltip("Invoked whenever the SFX slider gains focus.")] 
        private UnityEvent sfxSliderGainedFocus;
        [SerializeField, Tooltip("Invoked whenever the Music slider gains focus.")] 
        private UnityEvent musicSliderGainedFocus;
        [SerializeField, Tooltip("Invoked whenever the Gamma slider gains focus.")] 
        private UnityEvent gammaSliderGainedFocus;
        [SerializeField, Tooltip("Invoked whenever the Fullscreen toggle box gains focus.")] 
        private UnityEvent fullscreenToggleGainedFocus;
        [SerializeField, Tooltip("Invoked whenever the Player Skin dropdown field gains focus.")] 
        private UnityEvent backButtonGainedFocus;
        
        [Header("Confirmation")]
        [SerializeField, Tooltip("Invoked whenever the Back button is confirmed (clicked or tapped).")] 
        private UnityEvent backButtonConfirmed;
        
        /// <summary>
        /// The options menu as a whole.
        /// </summary>
        private VisualElement _optionsMenu;
    
        /// <summary>
        /// The PlayerSkin dropdown field.
        /// </summary>
        private DropdownField _dropdownPlayerSkin;
        
        /// <summary>
        /// The Master audio slider.
        /// </summary>
        private Slider _sliderAudioMaster;
        /// <summary>
        /// The SFX audio slider.
        /// </summary>
        private Slider _sliderAudioSfx;
        /// <summary>
        /// The Music audio slider.
        /// </summary>
        private Slider _sliderAudioMusic;
    
        /// <summary>
        /// The Gamma video slider.
        /// </summary>
        private Slider _sliderVideoGamma;
    
        /// <summary>
        /// The Fullscreen toggle option.
        /// </summary>
        private Toggle _toggleFullscreen;
    
        /// <summary>
        /// The back button. Will generally lead the user back to the main menu.
        /// </summary>
        private Button _buttonBack;
    
        /// <summary>
        /// Initializes variables.
        /// </summary>
        private void Awake()
        {
            _optionsMenu = GetComponent<UIDocument>().rootVisualElement;
        }
        
        /// <summary>
        /// Sets up callbacks.
        /// </summary>
        private void Start()
        {
            _dropdownPlayerSkin = _optionsMenu.Q<DropdownField>("PlayerSkin");
            _dropdownPlayerSkin.value = PlayerPrefs.GetString("PlayerSkin");
            _dropdownPlayerSkin.RegisterCallback<FocusInEvent>(evt => OnPlayerSkinDropdownFieldGainedFocus());
            _dropdownPlayerSkin.RegisterValueChangedCallback(delegate(ChangeEvent<string> evt)
            {
                PlayerPrefs.SetString("PlayerSkin", evt.newValue);
                _buttonBack.Focus();
            });
            _dropdownPlayerSkin.RegisterCallback<ClickEvent>(delegate(ClickEvent evt)
            {
                if (evt.altKey)
                {
                    _dropdownPlayerSkin.index = 0;
                }
            });
    
            _sliderAudioMaster = _optionsMenu.Q<Slider>("Master");
            _sliderAudioMaster.RegisterCallback<FocusInEvent>(evt => OnMasterSliderGainedFocus());
            _sliderAudioMaster.RegisterValueChangedCallback(evt => masterSliderValueChanged.Invoke(evt.newValue));
            _sliderAudioMaster.RegisterCallback<ClickEvent>(delegate(ClickEvent evt)
            {
                if (evt.altKey)
                {
                    _sliderAudioMaster.value = DefaultAudioSliderValue;
                }
            });
            
            _sliderAudioSfx = _optionsMenu.Q<Slider>("SFX");
            _sliderAudioSfx.RegisterCallback<FocusInEvent>(evt => OnSfxSliderGainedFocus());
            _sliderAudioSfx.RegisterValueChangedCallback(evt => sfxSliderValueChanged.Invoke(evt.newValue));
            _sliderAudioSfx.RegisterCallback<ClickEvent>(delegate(ClickEvent evt)
            {
                if (evt.altKey)
                {
                    _sliderAudioSfx.value = DefaultAudioSliderValue;
                }
            });
    
            _sliderAudioMusic = _optionsMenu.Q<Slider>("Music");
            _sliderAudioMusic.RegisterCallback<FocusInEvent>(evt => OnMusicSliderGainedFocus());
            _sliderAudioMusic.RegisterValueChangedCallback(evt => musicSliderValueChanged.Invoke(evt.newValue));
            _sliderAudioMusic.RegisterCallback<ClickEvent>(delegate(ClickEvent evt)
            {
                if (evt.altKey)
                {
                    _sliderAudioMusic.value = DefaultAudioSliderValue;
                }
            });
            
            _sliderVideoGamma = _optionsMenu.Q<Slider>("Gamma");
            _sliderVideoGamma.RegisterCallback<FocusInEvent>(evt => OnGammaSliderGainedFocus());
            _sliderVideoGamma.RegisterValueChangedCallback(evt => RenderSettings.ambientIntensity = evt.newValue);
            _sliderVideoGamma.RegisterCallback<ClickEvent>(delegate(ClickEvent evt)
            {
                if (evt.altKey)
                {
                    _sliderVideoGamma.value = DefaultGammaSliderValue;
                }
            });
    
            _toggleFullscreen = _optionsMenu.Q<Toggle>("Fullscreen");
            _toggleFullscreen.RegisterCallback<FocusInEvent>(evt => OnFullscreenToggleGainedFocus());
            _toggleFullscreen.RegisterValueChangedCallback(evt => Screen.fullScreen = evt.newValue);
    
            _buttonBack = _optionsMenu.Q<Button>("Back");
            _buttonBack.RegisterCallback<NavigationSubmitEvent>(evt => OnBackButtonConfirmed());
            _buttonBack.RegisterCallback<FocusInEvent>(evt => OnBackButtonGainedFocus());
            _buttonBack.RegisterCallback<FocusOutEvent>(evt => _buttonBack.style.opacity = buttonOpacityUnfocused);
            _buttonBack.RegisterCallback<ClickEvent>(evt => OnBackButtonConfirmed());
            _buttonBack.RegisterCallback<PointerOutEvent>(evt => _buttonBack.Blur());
            _buttonBack.RegisterCallback<PointerEnterEvent>(delegate(PointerEnterEvent evt)
            {
                _buttonBack.Blur(); // This makes sure the user can swap between KBM and Gamepad without UI bugs.
                _buttonBack.Focus();
            });
            
            SetVisibility(willStartVisible);
        }
    
        /// <summary>
        /// Called whenever the PlayerSkin dropdown field gains focus.
        /// </summary>
        private void OnPlayerSkinDropdownFieldGainedFocus()
        {
            Debug.Log($"OptionsMenu: Skin dropdown-field gained focus.");
            playerSkinDropdownFieldGainedFocus.Invoke();
        }
        
        /// <summary>
        /// Called whenever the Master audio slider gains focus.
        /// </summary>
        private void OnMasterSliderGainedFocus()
        {
            Debug.Log($"OptionsMenu: Master slider gained focus.");
            masterSliderGainedFocus.Invoke();
        }
    
        /// <summary>
        /// Called whenever the SFX audio slider gains focus.
        /// </summary>
        private void OnSfxSliderGainedFocus()
        {
            Debug.Log($"OptionsMenu: SFX slider gained focus.");
            sfxSliderGainedFocus.Invoke();;
        }
    
        /// <summary>
        /// Called whenever the Music audio slider gains focus.
        /// </summary>
        private void OnMusicSliderGainedFocus()
        {
            Debug.Log($"OptionsMenu: Music slider gained focus.");
            musicSliderGainedFocus.Invoke();
        }
    
        /// <summary>
        /// Called whenever the Gamma video slider gains focus.
        /// </summary>
        private void OnGammaSliderGainedFocus()
        {
            Debug.Log($"OptionsMenu: Gamma slider gained focus.");
            gammaSliderGainedFocus.Invoke();
        }
    
        /// <summary>
        /// Called whenever the Fullscreen toggle option gains focus.
        /// </summary>
        private void OnFullscreenToggleGainedFocus()
        {
            Debug.Log($"OptionsMenu: Fullscreen toggle gained focus.");
            fullscreenToggleGainedFocus.Invoke();
        }
    
        /// <summary>
        /// Called whenever the Back button gains focus.
        /// </summary>
        private void OnBackButtonGainedFocus()
        {
            Debug.Log($"OptionsMenu: Back button gained focus.");
            backButtonGainedFocus.Invoke();
            
            _buttonBack.style.opacity = buttonOpacityFocused;
        }
    
        /// <summary>
        /// Called whenever the Back button is confirmed.
        /// </summary>
        private void OnBackButtonConfirmed()
        {
            Debug.Log($"OptionsMenu: Back button confirmed.");
            backButtonConfirmed.Invoke();
        }
        
        /// <summary>
        /// Sets the menu visibility.
        /// </summary>
        /// <param name="visibility">Whether or not the menu should be visible.</param>
        public void SetVisibility(bool visibility)
        {
            _optionsMenu.visible = visibility;
        }
    }
}
