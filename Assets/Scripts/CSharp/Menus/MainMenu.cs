/*
 * MainMenu.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Lapis.Helpers;

namespace Lapis.Menus
{
    /// <summary>
    /// The main menu manager. Handles event callbacks and other affairs.
    /// </summary>
    public class MainMenu : MonoBehaviour, IVisibility // TODO: create parent Menu class
    {
        [Header("State")] 
        [SerializeField, Tooltip("Whether or not this menu should be readily visible.")] 
        private bool willStartVisible = true;
        
        [Header("Flow")] 
        [SerializeField, Tooltip("The name of the scene to load.")] 
        private string levelScene = "";
    
        [Header("Style")]
        [SerializeField, Tooltip("The opacity of the button when focused.")] 
        float buttonOpacityFocused = 1.0f;
        [SerializeField, Tooltip("The original opacity of the buttons.")] 
        float buttonOpacityUnfocused = 0.5f;
        
        [Header("Confirm")]
        [SerializeField, Tooltip("The play button confirmation event.")] 
        private UnityEvent playButtonConfirmed;
        [SerializeField, Tooltip("The options button confirmation event.")] 
        private UnityEvent optionsButtonConfirmed;
        [SerializeField, Tooltip("The quit button confirmation event.")] 
        private UnityEvent quitButtonConfirmed;
        
        [Header("Focus")]
        [SerializeField, Tooltip("The play button focus (and hover) event.")] 
        private UnityEvent playButtonGainedFocus;
        [SerializeField, Tooltip("The options button focus (and hover) event.")] 
        private UnityEvent optionsButtonGainedFocus;
        [SerializeField, Tooltip("The quit button focus (and hover) event.")] 
        private UnityEvent quitButtonGainedFocus;
        
        /// <summary>
        /// The Main Menu as a whole.
        /// </summary>
        private VisualElement _mainMenu;
        /// <summary>
        /// The Play button.
        /// </summary>
        private Button _buttonPlay;
        /// <summary>
        /// The Options button.
        /// </summary>
        private Button _buttonOptions;
        /// <summary>
        /// The Quit button.
        /// </summary>
        private Button _buttonQuit;
    
        /// <summary>
        /// Initializes variables.
        /// </summary>
        private void Awake()
        {
            _mainMenu = GetComponent<UIDocument>().rootVisualElement;
        }
    
        /// <summary>
        /// Sets up callbacks.
        /// </summary>
        private void Start()
        {
            _buttonPlay = _mainMenu.Q<Button>("Play");
            _buttonPlay.RegisterCallback<NavigationSubmitEvent>(evt => OnPlayButtonConfirmed());
            _buttonPlay.RegisterCallback<FocusInEvent>(evt => OnPlayButtonGainedFocus());
            _buttonPlay.RegisterCallback<FocusOutEvent>(evt => _buttonPlay.style.opacity = buttonOpacityUnfocused);
            _buttonPlay.RegisterCallback<ClickEvent>(evt => OnPlayButtonConfirmed());
            _buttonPlay.RegisterCallback<PointerOutEvent>(evt => _buttonPlay.Blur());
            _buttonPlay.RegisterCallback<PointerEnterEvent>(delegate(PointerEnterEvent evt)
            {
                _buttonPlay.Blur(); // This makes sure the user can swap between KBM and Gamepad without UI bugs.
                _buttonPlay.Focus();
            });
    
            _buttonOptions = _mainMenu.Q<Button>("Options");
            _buttonOptions.RegisterCallback<NavigationSubmitEvent>(evt => OnOptionsButtonConfirmed());
            _buttonOptions.RegisterCallback<FocusInEvent>(evt => OnOptionsButtonGainedFocus());
            _buttonOptions.RegisterCallback<FocusOutEvent>(evt => _buttonOptions.style.opacity = buttonOpacityUnfocused);
            _buttonOptions.RegisterCallback<ClickEvent>(evt => OnOptionsButtonConfirmed());
            _buttonOptions.RegisterCallback<PointerEnterEvent>(evt => _buttonOptions.Focus());
            _buttonOptions.RegisterCallback<PointerOutEvent>(evt => _buttonOptions.Blur());
    
            _buttonQuit = _mainMenu.Q<Button>("Quit");
            _buttonQuit.RegisterCallback<NavigationSubmitEvent>(evt => OnQuitButtonConfirmed());
            _buttonQuit.RegisterCallback<FocusInEvent>(evt => OnQuitButtonGainedFocus());
            _buttonQuit.RegisterCallback<FocusOutEvent>(evt => _buttonQuit.style.opacity = buttonOpacityUnfocused);
            _buttonQuit.RegisterCallback<ClickEvent>(evt => OnQuitButtonConfirmed());
            _buttonQuit.RegisterCallback<PointerEnterEvent>(evt => _buttonQuit.Focus());
            _buttonQuit.RegisterCallback<PointerOutEvent>(evt => _buttonQuit.Blur());
            
            SetVisibility(willStartVisible);
        }
    
        /// <summary>
        /// Event handler for clicking or submitting (pressing a button) on the play button.
        /// </summary>
        private void OnPlayButtonConfirmed()
        {
            Debug.Log($"MainMenu: Play button confirmed.");
            playButtonConfirmed.Invoke();
            
            SceneManager.LoadScene(levelScene);
        }
    
        /// <summary>
        /// Event handler for clicking or submitting (pressing a button) on the options button.
        /// </summary>
        private void OnOptionsButtonConfirmed()
        {
            Debug.Log($"MainMenu: Options button confirmed.");
            optionsButtonConfirmed.Invoke();
            
            // SetVisibility(false);
        }
    
        /// <summary>
        /// Event handler for clicking or submitting (pressing a button) on the quit button.
        /// </summary>
        private void OnQuitButtonConfirmed()
        {
            Debug.Log($"MainMenu: Quit button confirmed.");
            quitButtonConfirmed.Invoke();
            
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
        
        /// <summary>
        /// Event handler for focusing (including hovering) on the play button.
        /// </summary>
        private void OnPlayButtonGainedFocus()
        {
            Debug.Log($"MainMenu: Play button gained focus.");
            playButtonGainedFocus.Invoke();
            
            _buttonPlay.style.opacity = buttonOpacityFocused;
        }
    
        /// <summary>
        /// Event handler for focusing (including hovering) on the options button.
        /// </summary>
        private void OnOptionsButtonGainedFocus()
        {
            Debug.Log($"MainMenu: Options button gained focus.");
            optionsButtonGainedFocus.Invoke();
            
            _buttonOptions.style.opacity = buttonOpacityFocused;
        }
    
        /// <summary>
        /// Event handler for focusing (including hovering) on the quit button.
        /// </summary>
        private void OnQuitButtonGainedFocus()
        {
            Debug.Log($"MainMenu: Quit button gained focus.");
            quitButtonGainedFocus.Invoke();
            
            _buttonQuit.style.opacity = buttonOpacityFocused;
        }
    
        /// <summary>
        /// Sets the menu visibility.
        /// </summary>
        /// <param name="visibility">Whether or not the menu should be visible.</param>
        public void SetVisibility(bool visibility)
        {
            _mainMenu.visible = visibility;
        }
    }
}
