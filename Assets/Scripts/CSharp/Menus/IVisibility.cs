/*
 * IVisibility.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

namespace Lapis.Helpers
{
    /// <summary>
    /// Helper interface intended for menus. This was made to be replace SetActive on GameObjects with UI components,
    /// but may be repurposed for other goals.
    /// </summary>
    public interface IVisibility
    {
        /// <summary>
        /// Sets the menu visibility.
        /// </summary>
        /// <param name="visibility">Whether or not the menu should be visible.</param>
        public void SetVisibility(bool visibility);
    }
}

