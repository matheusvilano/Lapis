/*
 * MainMenu.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Lapis.Helpers;
using Lapis.GameModes;

namespace Lapis.Menus
{
    /// <summary>
    /// The main menu manager. Handles event callbacks and other affairs.
    /// </summary>
    public class ReviewMenu : MonoBehaviour, IVisibility // TODO: create parent Menu class
    {
        [Header("State")] 
        [SerializeField, Tooltip("Whether or not this menu should be readily visible.")] 
        private bool willStartVisible = false;
        
        [Header("Flow")] 
        [SerializeField, Tooltip("The name of the scene to load.")] 
        private string levelScene = "Sample";
        [SerializeField, Tooltip("The name of the scene to load.")] 
        private string menuScene = "MainMenu";

        [Header("Style")]
        [SerializeField, Tooltip("The opacity of the button when focused.")] 
        private float buttonOpacityFocused = 1.0f;
        [SerializeField, Tooltip("The original opacity of the buttons.")] 
        private float buttonOpacityUnfocused = 0.5f;
        
        [Header("Confirm")]
        [SerializeField, Tooltip("The restart button confirmation event.")] 
        private UnityEvent restartButtonConfirmed;
        [SerializeField, Tooltip("The exit button confirmation event.")] 
        private UnityEvent exitButtonConfirmed;

        [Header("Focus")]
        [SerializeField, Tooltip("The restart button focus (and hover) event.")] 
        private UnityEvent restartButtonGainedFocus;
        [SerializeField, Tooltip("The exit button focus (and hover) event.")] 
        private UnityEvent exitButtonGainedFocus;

        /// <summary>
        /// The Main Menu as a whole.
        /// </summary>
        private VisualElement _mainMenu;
        /// <summary>
        /// The Time (score) label.
        /// </summary>
        private Label _labelTime;
        /// <summary>
        /// The Restart button.
        /// </summary>
        private Button _buttonRestart;
        /// <summary>
        /// The Exit button.
        /// </summary>
        private Button _buttonExit;

        /// <summary>
        /// Initializes variables.
        /// </summary>
        private void Awake()
        {
            _mainMenu = GetComponent<UIDocument>().rootVisualElement;
        }
    
        /// <summary>
        /// Sets up callbacks.
        /// </summary>
        private void Start()
        {
            // TODO: make this change depending on the game mode, or create different menus.
            var timeAttackGameMode = GameMode.Instance as TimeAttackPlatformer;

            if (timeAttackGameMode != null)
            {
                _labelTime = _mainMenu.Q<Label>("Time");
                _labelTime.text = timeAttackGameMode.TimeElapsed.ToString();
            }
            
            _buttonRestart = _mainMenu.Q<Button>("Restart");
            _buttonRestart.RegisterCallback<NavigationSubmitEvent>(evt => OnRestartButtonConfirmed());
            _buttonRestart.RegisterCallback<FocusInEvent>(evt => OnRestartButtonGainedFocus());
            _buttonRestart.RegisterCallback<FocusOutEvent>(evt => _buttonRestart.style.opacity = buttonOpacityUnfocused);
            _buttonRestart.RegisterCallback<ClickEvent>(evt => OnRestartButtonConfirmed());
            _buttonRestart.RegisterCallback<PointerOutEvent>(evt => _buttonRestart.Blur());
            _buttonRestart.RegisterCallback<PointerEnterEvent>(delegate(PointerEnterEvent evt)
            {
                _buttonRestart.Blur(); // This makes sure the user can swap between KBM and Gamepad without UI bugs.
                _buttonRestart.Focus();
            });
    
            _buttonExit = _mainMenu.Q<Button>("Exit");
            _buttonExit.RegisterCallback<NavigationSubmitEvent>(evt => OnExitButtonConfirmed());
            _buttonExit.RegisterCallback<FocusInEvent>(evt => OnExitButtonGainedFocus());
            _buttonExit.RegisterCallback<FocusOutEvent>(evt => _buttonExit.style.opacity = buttonOpacityUnfocused);
            _buttonExit.RegisterCallback<ClickEvent>(evt => OnExitButtonConfirmed());
            _buttonExit.RegisterCallback<PointerEnterEvent>(evt => _buttonExit.Focus());
            _buttonExit.RegisterCallback<PointerOutEvent>(evt => _buttonExit.Blur());

            SetVisibility(willStartVisible);
        }
    
        /// <summary>
        /// Event handler for clicking or submitting (pressing a button) on the play button.
        /// </summary>
        private void OnRestartButtonConfirmed()
        {
            Debug.Log($"ReviewMenu: Restart button confirmed.");
            restartButtonConfirmed.Invoke();
            
            SceneManager.LoadScene(levelScene);
        }
    
        /// <summary>
        /// Event handler for clicking or submitting (pressing a button) on the options button.
        /// </summary>
        private void OnExitButtonConfirmed()
        {
            Debug.Log($"ReviewMenu: Exit button confirmed.");
            exitButtonConfirmed.Invoke();
            
            SceneManager.LoadScene(menuScene);
        }

        /// <summary>
        /// Event handler for focusing (including hovering) on the play button.
        /// </summary>
        private void OnRestartButtonGainedFocus()
        {
            Debug.Log($"ReviewMenu: Restart button gained focus.");
            restartButtonGainedFocus.Invoke();
            
            _buttonRestart.style.opacity = buttonOpacityFocused;
        }
    
        /// <summary>
        /// Event handler for focusing (including hovering) on the options button.
        /// </summary>
        private void OnExitButtonGainedFocus()
        {
            Debug.Log($"ReviewMenu: Exit button gained focus.");
            exitButtonGainedFocus.Invoke();
            
            _buttonExit.style.opacity = buttonOpacityFocused;
        }

        /// <summary>
        /// Sets the menu visibility.
        /// </summary>
        /// <param name="visibility">Whether or not the menu should be visible.</param>
        public void SetVisibility(bool visibility)
        {
            _mainMenu.visible = visibility;
        }
    }
}
