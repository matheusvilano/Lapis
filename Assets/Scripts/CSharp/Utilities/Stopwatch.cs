/*
 * Stopwatch.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using Lapis.GameModes;

namespace Lapis.Utilities
{
    /// <summary>
    /// This class simulates the basic functionalities of a Stopwatch, but based on GameStates (no manual start/stop).
    /// </summary>
    [
        SuppressMessage("ReSharper", "PossibleLossOfFraction"), 
        SuppressMessage("ReSharper", "MemberCanBePrivate.Global")
    ]
    public class Stopwatch
    {
        /// <summary>
        /// The amount of minutes in an hour.
        /// </summary>
        public const int MinutesInHour = 60;
        /// <summary>
        /// The amount of seconds in a minute.
        /// </summary>
        public const int SecondsInMinute = 60;
        /// <summary>
        /// The amount of milliseconds in a second.
        /// </summary>
        public const int MillisecondsInSecond = 1000;
    
        /// <summary>
        /// The current amount of hours.
        /// </summary>
        private int _hours;
        /// <summary>
        /// The current amount of minutes.
        /// </summary>
        private int _minutes;
        /// <summary>
        /// The current amount of seconds.
        /// </summary>
        private int _seconds;
        /// <summary>
        /// The current amount of milliseconds.
        /// </summary>
        /// <remarks>This is the only float field.</remarks>
        private float _milliseconds;
    
        /// <summary>
        /// The current amount of hours.
        /// </summary>
        public int Hours 
        { 
            get => _hours;
            private set
            {
                if (value >= 0)
                {
                    _hours = value;
                }
            }
        }
        
        /// <summary>
        /// The current amount of minutes.
        /// </summary>
        public int Minutes
        { 
            get => _minutes;
            private set
            {
                if (value < 0) return;
                if (value < MinutesInHour)
                {
                    _minutes = value;
                }
                else
                {
                    int amountOfHours = (int)Mathf.Floor(value / MinutesInHour);
    
                    _minutes %= (int)Mathf.Floor(value / MinutesInHour);
                    Hours += amountOfHours;
                }
            }
        }
    
        /// <summary>
        /// The current amount of seconds.
        /// </summary>
        public int Seconds 
        { 
            get => _seconds;
            private set
            {
                if (value >= 0)
                {
                    if (value < SecondsInMinute)
                    {
                        _seconds = value;
                    }
                    else
                    {
                        int amountOfMinutes = (int)Mathf.Floor(value / SecondsInMinute);
    
                        _seconds %= (int)Mathf.Floor(value / SecondsInMinute);
                        Minutes += amountOfMinutes;
                    }
                }
            }
        }
    
        /// <summary>
        /// The current amount of milliseconds.
        /// </summary>
        /// <remarks>This is the only float field.</remarks>
        public float Milliseconds
        { 
            get => _milliseconds;
            set
            {
                if (value <= 0) return;
                if (value < MillisecondsInSecond)
                {
                    _milliseconds = value;
                }
                else
                {
                    int amountOfSeconds = (int)Mathf.Floor(value / MillisecondsInSecond);
    
                    _milliseconds %= (int)Mathf.Floor(value / MillisecondsInSecond);
                    Seconds += amountOfSeconds;
                }
            }
        }
    
        /// <summary>
        /// Add time (in milliseconds).
        /// </summary>
        /// <param name="millisecondsAddend">The amount of time (in milliseconds) to add.</param>
        public void AddMilliseconds(in float millisecondsAddend)
        {
            if (GameMode.Instance.GameState == GameStates.Playing) // TODO: Make sure the GameState is changing.
            {
                Milliseconds += millisecondsAddend;
            }
        }
    
        /// <summary>
        /// Converts the time to a properly-formatted string.
        /// </summary>
        /// <returns>The time as a properly-formatted string.</returns>
        public override string ToString()
        {
            const int millisecondFormatDivisor = 10;
            return $"Time: {_hours}:{_minutes}:{_seconds}.{(int)Mathf.Floor(_milliseconds / millisecondFormatDivisor)}";
        }
    }
}
