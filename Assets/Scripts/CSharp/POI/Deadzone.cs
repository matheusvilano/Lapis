/*
 * Deadzone.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Lapis.GameModes;
using Lapis.GameModes.Interfaces;
using UnityEngine;

namespace Lapis.Utilities
{
    /// <summary>
    /// This class handles respawning the player when it goes out of bounds.
    /// </summary>
    [RequireComponent(typeof(BoxCollider))]
    public class Deadzone : MonoBehaviour
    {
        /// <summary>
        /// Forces the box collider to be a trigger.
        /// </summary>
        private void Awake()
        {
            GetComponent<BoxCollider>().isTrigger = true;
        }

        /// <summary>
        /// Respawn the overlapping object if it is the player.
        /// </summary>
        /// <param name="other">The colliding object.</param>
        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player")) return;
            
            Debug.Log($"Deadzone: Player is out of bounds and will respawn if the current Game Mode supports checkpoints.");
            
            if (GameMode.Instance is ICheckpoint gameMode)
            {
                gameMode.Respawn();
            }
        }
    }
}
