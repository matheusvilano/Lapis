/*
 * Checkpoint.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;
using UnityEngine.Events;
using Lapis.GameModes.Interfaces;

namespace Lapis.GameModes
{
    /// <summary>
    /// This class represents a checkpoint in a level, where the player can respawn.
    /// </summary>
    [RequireComponent(typeof(BoxCollider))]
    public class Checkpoint : MonoBehaviour
    {
        /// <summary>
        /// The amount of times a checkpoint has been reached. Works best when all checkpoints are set to auto-destroy.
        /// </summary>
        private static uint _checkpointsReached;

        [Header("Settings")]
        [SerializeField, Tooltip("Whether or not this checkpoint will destroy itself after the Player goes past it.")]
        private bool willSelfDestroyOnOverlap = true;

        [Header("Events")] [SerializeField, Tooltip("Invoked whenever the Player overlaps with this checkpoint.")]
        private UnityEvent<uint> checkpointReached;

        /// <summary>
        /// Whether or not this checkpoint has been reached yet.
        /// </summary>
        private bool _wasCheckpointReached;

        /// <summary>
        /// Used to force the collider to become a trigger.
        /// </summary>
        private void Awake()
        {
            GetComponent<BoxCollider>().isTrigger = true; // Force the collider to become a trigger.
        }

        /// <summary>
        /// Checks if the collider is the Player.
        /// </summary>
        /// <param name="other">The colliding object.</param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                OnCheckpointReached();
            }
        }

        /// <summary>
        /// Called whenever the Player begins overlapping the checkpoint.
        /// </summary>
        private void OnCheckpointReached()
        {
            if (_wasCheckpointReached) return; // Early-out if this checkpoint has already been reached.

            checkpointReached.Invoke(++_checkpointsReached);

            if (GameMode.Instance is ICheckpoint gameMode)
            {
                gameMode.SetCheckpoint(transform);
                Debug.Log($"Checkpoint: Reached checkpoint {gameObject.name} at {transform.position}.");
            }

            if (willSelfDestroyOnOverlap)
            {
                Destroy(gameObject);
            }
            else
            {
                _wasCheckpointReached = true;
            }
        }
    }
}
