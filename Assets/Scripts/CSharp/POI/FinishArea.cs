/*
 * FinishArea.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;
using UnityEngine.Events;
using Lapis.GameModes;

namespace Lapis.Utilities
{
    /// <summary>
    /// This class defines the common behaviours for a trigger shape defined as a "finish area".
    /// </summary>
    public class FinishArea : MonoBehaviour
    {
        [Header("Events")]
        [SerializeField, Tooltip("Event that gets invoked when the Player overlaps with the Finish Area.")]
        private UnityEvent playerReachedFinishArea;

        /// <summary>
        /// Invoke an event if the collider game object is the Player.
        /// </summary>
        /// <param name="other">The other colliding object.</param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == GameMode.Instance.Player)
            {
                playerReachedFinishArea.Invoke();
            }
        }
    }
}
