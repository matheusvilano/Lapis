/*
 * ICheckpoint.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;

namespace Lapis.GameModes.Interfaces
{
    /// <summary>
    /// Interface class for game modes that contain checkpoints.
    /// </summary>
    public interface ICheckpoint
    {
        /// <summary>
        /// Sets a new checkpoint.
        /// </summary>
        public void SetCheckpoint(Transform checkpoint);

        /// <summary>
        /// Respawns the player.
        /// </summary>
        public void Respawn();
    }
}
