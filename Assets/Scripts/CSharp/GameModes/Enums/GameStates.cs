/*
 * GameStates.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

namespace Lapis.GameModes
{
    /// <summary>
    /// General game states applicable to any game mode in Lapis.
    /// </summary>
    public enum GameStates
    {
        /// <summary>
        /// The game is loading.
        /// </summary>
        Loading,
        
        /// <summary>
        /// The game is currently playing.
        /// </summary>
        Playing,
        
        /// <summary>
        /// The game is paused.
        /// </summary>
        Paused,
        
        /// <summary>
        /// The game has finished.
        /// </summary>
        Finished
    }
}
