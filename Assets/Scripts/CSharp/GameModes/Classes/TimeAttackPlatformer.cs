/*
 * TimeAttackPlatformer.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using Lapis.GameModes.Interfaces;
using Lapis.Utilities;
using UnityEngine.SceneManagement;

namespace Lapis.GameModes
{
    /// <summary>
    /// Set of rules for the Time Attack platformer mode.
    /// </summary>
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class TimeAttackPlatformer : GameMode, ICheckpoint
    {
        #region Fields

        [Header("Stopwatch")]
        
        [SerializeField, Tooltip("How often the Stopwatch should be updated. Avoid changing.")] 
        private float refreshRateInSeconds = 0.01f;

        [Header("POI")]
        
        [SerializeField, Tooltip("The area where the player will start the level.")] 
        private GameObject startingPoint;

        [SerializeField, Tooltip("The area where the player will finish the level (OnTriggerEnter).")] 
        private GameObject endingPoint;
        
        [Header("GameState")]
        
        [SerializeField, Tooltip("Event invoked whenever the game state has changed.")] 
        private UnityEvent<GameStates> gameStateChanged;

        /// <summary>
        /// The position where the player may respawn.
        /// </summary>
        private Vector3 _checkpoint;
        
        /// <summary>
        /// The time elapsed since the start of the game.
        /// </summary>
        private Stopwatch _timeElapsed;
        
        #endregion
        
        #region Properties

        /// <summary>
        /// The time elapsed since the start of the game.
        /// </summary>
        public Stopwatch TimeElapsed
        {
            get => _timeElapsed;
            private set
            {
                if (value != null)
                {
                    _timeElapsed = value;
                }
            }
        }

        #endregion

        #region Monobehaviours

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Home))
            {
                Respawn();
            }
            
            Debug.Log(TimeElapsed.ToString());
        }

        /// <summary>
        /// Called when the script instance is being loaded.
        /// </summary>
        private new void Awake()
        {
            base.Awake();
            TimeElapsed = new Stopwatch();

            _checkpoint = startingPoint != null ? startingPoint.transform.position : Player.transform.position;
        }

        /// <summary>
        /// This function runs at the first frame of action.
        /// </summary>
        protected void Start()
        {
            // Plan B: if Player is null, make best efforts to assign it to something.
            if (Player == null)
            {
                Player = GameObject.FindWithTag("Player") ?? GameObject.Find("Player");
            }

            // Update TimeElapsed
            InvokeRepeating(nameof(IncreaseTimeElapsed), 0.0f, refreshRateInSeconds);
        }
        
        #endregion

        #region Methods

        /// <summary>
        /// Sets the new checkpoint.
        /// </summary>
        /// <param name="checkpoint">The new checkpoint.</param>
        public void SetCheckpoint(Transform checkpoint)
        {
            // Make sure the transform belongs to a Checkpoint game object.
            if (checkpoint.GetComponent<Checkpoint>() != null)
            {
                _checkpoint = checkpoint.position;
            }
        }
        
        /// <summary>
        /// Respawns the player.
        /// </summary>
        public void Respawn()
        {
            Player.transform.position = _checkpoint;
            CustomEvent.Trigger(Player, "Respawn"); // Visual Scripting
        }
        
        /// <summary>
        /// This functions runs at framerate.
        /// </summary>
        protected void IncreaseTimeElapsed()
        {
            TimeElapsed.AddMilliseconds(Stopwatch.MillisecondsInSecond * refreshRateInSeconds);
        }

        /// <summary>
        /// Event handler for when the player reaches the finish area.
        /// </summary>
        public void OnPlayerReachedFinishArea()
        {
            // TODO: Implement a win condition here.
            GameState = GameStates.Finished;
            
            // TODO: do not destroy the player; prevent input instead.
            // TODO: do not reload scene automatically; use menu instead.
            Player.SetActive(false);
            Invoke(nameof(RestartGame), 3.0f);
            
            Debug.Log($"{gameObject.name}: Level completed!");
        }

        // TODO: remove this function; player will reload scenes manually.
        private void RestartGame()
        {
            SceneManager.LoadScene("MainMenu");
        }
        
        /// <summary>
        /// Updates the game state.
        /// </summary>
        /// <param name="gameState">The new game state.</param>
        public void SetGameState(in GameStates gameState)
        {
            GameState = gameState;
            gameStateChanged.Invoke(gameState);
        }
        
        #endregion
    }
}
