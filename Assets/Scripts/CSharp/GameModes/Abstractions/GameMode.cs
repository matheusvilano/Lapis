/*
 * GameMode.cs
 * Written by Matheus Vilano
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 *
 * This work is used at Vancouver Film School, for the Unity Scripting course.
 */

using UnityEngine;

namespace Lapis.GameModes
{
    /// <summary>
    /// Base class for Game Modes.
    /// </summary>
    public abstract class GameMode : MonoBehaviour
    {
        [Header("Player")]
        [SerializeField, Tooltip("The currently-controlled player object.")] 
        private GameObject player;
        
        /// <summary>
        /// The currently-controlled player object.
        /// </summary>
        public GameObject Player
        {
            get => player;
            protected set => player = value != null ? value : player;
        }

        /// <summary>
        /// The current GameMode.
        /// </summary>
        public static GameMode Instance { get; private set; }

        /// <summary>
        /// The current Game State.
        /// </summary>
        public GameStates GameState { get; protected set; }

        /// <summary>
        /// Called when the script instance is being loaded.
        /// </summary>
        protected void Awake()
        {
            // Singleton pattern.
            if (Instance == null)
            {
                Instance = this;
                Debug.Log($"{gameObject.name}: Instance of a {Instance} game mode created.");
            }
            else
            {
                Destroy(this);
                Debug.LogWarning($"{gameObject.name}: Extra instance of a GameMode detected.");
            }
        }
    }
}
